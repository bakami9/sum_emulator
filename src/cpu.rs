
pub struct Registers {
    r0: u64,
    r1: u64,
    r2: u64,
    r3: u64,
    r4: u64,
    r5: u64,
    r6: u64,
    r7: u64,
    r8: u64,
    r9: u64,
    ip: u64,
    sp: u64,
    bp: u64,
}

impl Default for Registers {
    fn default() -> Self {
        Registers {
            r0: 0,
            r1: 0,
            r2: 0,
            r3: 0,
            r4: 0,
            r5: 0,
            r6: 0,
            r7: 0,
            r8: 0,
            r9: 0,

            //instruction pointer set to 0x1000 by default
            //>>execution after cpu start/reset starts here
            ip: 0x1000,
            
            //for stack frames
            sp: 0x0,
            bp: 0x0,
        }
    }
}
