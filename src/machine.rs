


pub trait Machine {
    //fn run_cycle(&mut self);

    fn mmu_write_value(&mut self);

    fn mmu_read_value(&self);

    //fn reg_read_value(&self);

    //fn reg_write_value(&mut self);

}
