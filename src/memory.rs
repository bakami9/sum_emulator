
//uhm we want to keep that stuff on the heap
//because otherwise the stack would overflow
//in no time :)
use std::boxed::Box;
use std::fmt::Debug;
use std::vec::Vec;

use std::fmt;

use crate::register_device::MemoryMapDevice;
use crate::debug::show_mapping;

pub struct Memory {
    video_memory: Box<[u8; 2048 * 2048]>,
    general_memory: Box<[u8; 0xffff]>,
}

impl Default for Memory {
    fn default() -> Self {
        Memory {
            //white :)
            video_memory: Box::new([0xff; 2048 * 2048]),
            general_memory: Box::new([0; 0xffff]),
        }
    }
}

impl MemoryMapDevice for &Memory {
    fn desired_locations(&self) -> Vec<(u64, u64)> {
        let mut ranges = Vec::new();
        //TODO generic types :||||||
        ranges.push( (0x1234,
                      (0x1234 + self.general_memory.len()-1) as u64));
                //map directly after one another
                //TODO offset checking
        ranges.push( (0x1234 + self.general_memory.len() as u64,
                      (0x4321 + self.video_memory.len()-1) as u64));
        ranges
    }

    fn assigned_locations(&self) -> Vec<(u64, u64)> {
        self.desired_locations()
    }

    fn human_identifier(&self) -> Option<String> {
        Some(String::from("VIRTUAL MEMORY BANK V0.0.1"))
    }
}


//TODO maybe this should dump more information than the memory
//mappings
impl fmt::Debug for Memory {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,"{}", show_mapping(self))
    }
}


