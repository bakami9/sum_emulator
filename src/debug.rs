use std::fmt;
use crate::register_device::MemoryMapDevice;

pub fn show_mapping(t: impl MemoryMapDevice) -> String {
    //TODO if not given derive from uuid like value thats sure to exist
    let mut rstr = format!("------memory mapping for \"{}\" ------\n",
                                t.human_identifier().unwrap_or(
                                    String::from("TODO dervice from uuid/mac/..")));
    let ranges = &t.assigned_locations();
    for (i, range) in ranges.iter().enumerate() {
        rstr.push_str(&format!(
                "mapping [{}]\t0x{:x} -> 0x{:x}\n",
                i, &range.0, &range.1) );
    }
    rstr.push_str("------memory mapping------");

    rstr
}
