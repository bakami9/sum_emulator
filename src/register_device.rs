

//add devices that wish to map themself
//into memory shall implement this trait
pub trait MemoryMapDevice {
    //shall retrun a vector with one or multiple
    //memory ranges that are wished to be allocated/mapped/etc
    //so the the memory management unit can decide if those
    //are even free/valid before initialisation
    fn desired_locations(&self) -> Vec<(u64, u64)>;
    
    fn assigned_locations(&self) -> Vec<(u64, u64)>;

    fn human_identifier(&self) -> Option<String>;

    //fn device_initialize() -> /*device specific init function*/,
    //fn device_deinitialize() -> /*device specific uninit function*/,

}
